//Colores para cada importancia de tarea, van enumerados por cada tipo de importancia
#define COLOR_1    "\033[0;31m" //rojo
#define COLOR_2        "\033[0;33m" //amarillo
#define COLOR_4    "\033[0;34m" //azul
#define COLOR_3   "\033[0;32m" //verde
#define ADVERTENCIA     "\033[5;31m" 
#define DEFECTO "\033[0;0m" 

//Cada tipo de color por importancia 
//1 a 20 = color_1	21 a 40 color_2		41 a 100 color_3	todo lo demas color_4
#define MOST_IMPORTANT  20
#define MIDDLE_IMPORTANT        40
#define LEAST_IMPORTANT 100
#define TAREA_BUFFER    128

//Ubicación del paja_db
#define PAJA_DB "/home/example/paja-db.txt"
//Ubicación de el paja_db temporal cuando se elimina una tarea.
#define PAJA_DB_TEMP "/home/example/.paja.temp"
//Buffer de cuantos chars puede leer paja. 512 es el minimo según yo, cambialo a tu gusto en relación a cuantas tareas crees que pondras.
#define PAJA_READ       512


