#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include "config.h"

int ayuda(char* in);
int escritura();
char* colores(int in);
int main(int argc, char* argv[]);
void NoNewline (char in[]);
int WriteToFile(char* data);
int ReadFileContents();
int DeleteString(int line);

struct option long_options[]=
{
	{"add",	no_argument,	0,	'a'},
	{"delete",	required_argument,	0,	'd'},
	{"help",	no_argument,	0,	'h'},
	{0,0,0,0}
};

int main(int argc, char* argv[])
{
	if (argc < 2) {ReadFileContents();return 0;}
	
	int opt, opt_index = 0;

	while((opt = getopt_long (argc,argv,":ahd:", long_options, &opt_index)) != -1) {

	switch(opt){
		case 'a':{escritura();}break;
		case 'd':{
			if(atoi(optarg) == 0){printf("Linea invalida\n"); exit(0);}
				DeleteString(atoi(optarg));}break;
		case 'h':{ayuda(argv[0]);}break;
		}
	}
	ReadFileContents();
	return 0;
}


int escritura()
{
	char importancia[5];
	char tarea[TAREA_BUFFER];
	char completo[TAREA_BUFFER + sizeof(importancia)+1 + 7]; // 7 por si las moscas lol 
	
	/* Tareas */
	printf("Tarea: ");
	char* Tarea_error = fgets(tarea, sizeof(tarea), stdin);
	if (Tarea_error == NULL){printf("Ha ocurrido un error con la tarea\n");exit(1);}
	else if (strlen(Tarea_error) > TAREA_BUFFER){printf("El tamaño de tu tarea es muy grande\n");exit(1);}

	printf("Importancia: ");
	char* Imp_error = fgets(importancia,sizeof(importancia), stdin);
	if (Imp_error == NULL){printf("Ha ocurrido un error con la importancia\n");exit(1);}
	else if (strlen(importancia) > sizeof(importancia)-1 ){printf("Muchos digitos en importancia");exit(1);}

	char* test = colores(atoi(importancia));
	NoNewline(tarea);

	printf("%s %d | %s  %s\n", test, atoi(importancia), tarea, DEFECTO);
	snprintf(completo, sizeof(completo), "%d | %s \n", atoi(importancia), tarea);
	WriteToFile(completo);

	return 0;
}

int WriteToFile(char* data)
{
	FILE* paja = fopen(PAJA_DB,"a");
	
	if(paja !=NULL){fputs(data, paja);}
	else { printf("ha ocurrido un error\n");
	exit(1);}

	fclose(paja);
	exit(0);
}

int ReadFileContents()
{
	int i = 1;
	char testeo[PAJA_READ];
	char* lol = (char*) malloc(PAJA_READ);
	
	FILE* paja = fopen(PAJA_DB, "r");
	if (!paja){printf("No existe un paja_db valido, usa paja --add para crear una tarea y el archivo.\n");exit(1);}
	printf("ID\tImp\t\tTarea\n");
	if(lol == NULL){printf("Algo salio mal\n");exit(1);}
	while(fgets(testeo,PAJA_READ, paja) != NULL){
		lol = strtok(testeo, "|");
		char* color = colores(atoi(lol));
		printf("%s%d", DEFECTO,i);
		i++;
		while (lol != NULL){
			printf("\t%s%-7s", color, lol);
			lol = strtok(NULL, "|");}
		}
	free(lol);
	fclose(paja);
	return 0;
}

int DeleteString(int line)
{
	char nuevo[PAJA_READ];
	int z = 0;
	FILE* paja = fopen(PAJA_DB, "r");
	if(!paja){printf("Paja_db no se ha encontrado\n"); exit(1);}
	FILE* Temp = fopen(PAJA_DB_TEMP, "w"); 
	if(!Temp){printf("No se logro crear el archivo temporal\n"); exit(1);}
	
	while(!feof(paja)){
		strcpy(nuevo, "\0");
		(void)!fgets(nuevo, PAJA_READ, paja);
		if (!feof(paja)){
			z++;
			if( z != line){fprintf(Temp, "%s", nuevo);}
			}
		}
	fclose(paja);
	fclose(Temp);
	remove(PAJA_DB);
	rename(PAJA_DB_TEMP, PAJA_DB);
	return 0;
}

void NoNewline (char in[])
{
	while(in[strlen(in)-1] == '\n'){in[strlen(in)-1] = '\0';}
}

char* colores (int in)
{
	if (in <= MOST_IMPORTANT && in != 0){return COLOR_1;}
	else if (in > MOST_IMPORTANT && in <= MIDDLE_IMPORTANT ){return COLOR_2;}
	else if (in > MIDDLE_IMPORTANT && in <= LEAST_IMPORTANT){return COLOR_3;}
	else{return COLOR_4;}
	
	return NULL;
}

int ayuda (char* in)
{
	printf("\nModo de uso:\n%s: [-a|--add] [-d|--delete] <ID> [-h|--help]\n\n--add agrega una tarea nueva.\n--delete Elimina una tarea existente.Se necesita especificar el numero de la tarea\n--help Muestra este mensaje de ayuda\n", in);
	exit(0);
}
