# paja-c

Simple todo manager

Se tiene que modificar config.h, ahi agregas el directorio donde quieres que se encuentre el paja_db y los colores

`gcc paja.c -o paja`

Modo de uso:
./paja: [-a|--add] [-d|--delete] <ID> [-h|--help]

--add agrega una tarea nueva.

--delete Elimina una tarea existente.Se necesita especificar el numero de la tarea

--help Muestra el mensaje de ayuda

Ejecutarlo sin argumentos muestra las tareas existentes, si no existe el archivo te dara un error (y no creara el paja_db, eso se hace con paja --add).
